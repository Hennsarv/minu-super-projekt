﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NWDemoRakendus.Startup))]
namespace NWDemoRakendus
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
